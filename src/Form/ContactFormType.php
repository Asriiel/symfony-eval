<?php

namespace App\Form;

use App\Entity\FormEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Civilite', ChoiceType::class, array(
                'choices' => array('Civilité' => 0, 'M' => 1, 'Mme' => 'Mme', 'Ne sais pas' => -1),
                'multiple' => false,
                'label' => false,
            ))
            ->add('Nom', TextType::class, array(
                'attr' => array(
                    'class' => 'form-group floating-label-form-group controls mb-0 pb-2',
                    'placeholder' => 'Nom',
                ),
                'label' => false,
            ))
            ->add('Prenom', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Prénom',
                ),
                'label' => false,
            ))
            ->add('Email', TextType::class, array(
                'attr' => array(
                    'pattern' => "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$",
                    'placeholder' => 'Email',
                ),
                'label' => false,
            ))
            ->add('Telephone', TextType::class, array(
                'attr' => array(
                    'pattern' => "^((\+)33|0)[1-9](\d{2}){4}$",
                    'placeholder' => 'Numéro de téléphone',
                ),
                'label' => false,
            ))
            ->add('newsletter', CheckboxType::class, array(
                'attr' => array(
                    'required' => false,
                    'placeholder' => 'Inscription à la newsletter',
                ),
                'required' => false,
                'label' => 'Inscription à la newsletter',
            ))
            ->add('save', SubmitType::class, array('label' => 'Enregistrer'))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormEntity::class,
            'csrf_protection' => false
        ]);
    }
}
