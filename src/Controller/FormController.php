<?php

namespace App\Controller;

use App\Entity\FormEntity;
use App\Form\ContactFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/form")
 */
class FormController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $formEntity = new FormEntity();
        $form = $this->createForm(ContactFormType::class, $formEntity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formEntity);
            $em->flush();
            return $this->redirectToRoute('index');
        }

        return $this->render('form/formPage.html.twig', [
            'controller_name' => 'FormController',
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/submit", name="submit")
     */
    public function submitAction(Request $request){
        $formEntity = new FormEntity();
        $form = $this->createForm(ContactFormType::class, $formEntity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($formEntity);
        $em->flush();

        return new JsonResponse(array(
            'status' => 'OK',
        ));
    }
}
