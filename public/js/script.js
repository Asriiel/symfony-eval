(function($) {
    "use strict";

    $("#myForm").submit(function(event){
        let form = $("#myForm");
        form[0].checkValidity(); // Pas eu le temps de faire la suite de la gestion d'erreur ici !
        event.preventDefault();
        console.log("IT WORKS !!!");
        let datastring = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "http://localhost:8000/form/submit",
            data: datastring,
            dataType: "json",
            success: function(data) {
                if (data.status === 'OK'){
                    console.log("Enregistrement en base réussi !");
                }
                $(".alertForm").html("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\n" +
                    "  <strong>Success !</strong> Enregistrement réussi !\n" +
                    "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                    "    <span aria-hidden=\"true\">&times;</span>\n" +
                    "  </button>\n" +
                    "</div>");
                $('#myForm').trigger("reset");
            },
            error: function() {
                console.log("Une erreur est survenu lors de l'enregistrement !");
                $(".alertForm").html("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">\n" +
                    "  <strong>Error !</strong> Enregistrement échoué !\n" +
                    "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                    "    <span aria-hidden=\"true\">&times;</span>\n" +
                    "  </button>\n" +
                    "</div>");
            }
        });
    });


})(jQuery); // End of use strict
